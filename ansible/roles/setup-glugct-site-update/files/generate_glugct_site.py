#!/usr/bin/env python3

import argparse
from git import Repo
import logging
import os
from pathlib import Path
import subprocess
import sys


def run_with_log(command, cwd):
    """Wrapper around subprocess.run which optionally uses
    logging to print stdout and stderr."""

    debug_enabled = logging.getLogger().getEffectiveLevel() == logging.DEBUG

    res = subprocess.run(command, cwd=cwd, capture_output=True)

    # only print the output when debug is enabled
    if debug_enabled:
        logging.debug(res.stdout)
        logging.debug(res.stderr)
    res.check_returncode()


def repo_need_refresh(repository_url, clone_dir):
    """Update the specified clone (or clone it if not available)
    and check whether is has been updated."""

    if not os.path.isdir(clone_dir):
        # The repository does not exist, it needs to be regenerated
        logging.debug('Cloning %s in %s' % (repository_url, clone_dir))
        repo = Repo.clone_from(repository_url, clone_dir)
        return False

    repo = Repo(clone_dir)

    # make sure to use master and reset it to a clean state
    repo.head.reference = repo.heads.master
    repo.head.reset(index=True, working_tree=True)

    # compare the current hash and the new hash after the update
    curr_hash = str(repo.head.reference.commit)

    repo.remote().pull()
    new_hash = str(repo.head.reference.commit)
    logging.debug('Old hash: %s, new hash: %s' % (curr_hash, new_hash))

    return curr_hash == new_hash


def generate_glugct_site(build_dir, repo_baseurl, canary_file):
    """Generate the website."""

    os.makedirs(build_dir, exist_ok=True)

    # whether each repository is up-to-date
    uptodate_status = {
        'pelican-vendored': False,
        'website': False
    }
    try:
        for repo in uptodate_status.keys():
            uptodate_status[repo] = (
                    repo_need_refresh(repo_baseurl+'/'+repo,
                                      os.path.join(build_dir, repo)))
    except Exception as exc:
        logging.error("Error while updating the repositories: %s" % (str(exc)))
        sys.exit(1)

    logging.debug('uptodate_status: %s' % (uptodate_status))

    if uptodate_status['pelican-vendored'] and uptodate_status['website']:
        # Nothing to do
        logging.debug('No updates needed')
        return

    try:
        work_dir = os.path.join(build_dir, 'pelican-vendored')
        run_with_log(['make', 'unapply_patches'], cwd=work_dir)
        run_with_log(['make', 'apply_patches'], cwd=work_dir)

        work_dir = os.path.join(build_dir, 'website')
        run_with_log(['make', 'clean'], cwd=work_dir)
        run_with_log(['make', 'html'], cwd=work_dir)
    except Exception as exc:
        logging.error("Error while refreshing the repositories: %s" %
                      (str(exc)))
        sys.exit(1)

    # create the canary file
    if canary_file:
        Path(canary_file).touch()


def main():
    parser = argparse.ArgumentParser('GLUGCT site updater')
    parser.add_argument('-d', '--debug', action="store_true",
                        dest="debug", default=False)
    parser.add_argument('-b', '--build-dir', action="store",
                        dest="build_dir", default=".")
    parser.add_argument('-r', '--repo-baseurl', action="store",
                        dest="repo_baseurl",
                        default="https://gitlab.com/glugcatania")
    parser.add_argument('-c', '--canary-file', action="store",
                        dest="canary_file", default=None)

    results = parser.parse_args()

    log_level = logging.INFO if not results.debug else logging.DEBUG
    logging.basicConfig(level=log_level)

    generate_glugct_site(results.build_dir, results.repo_baseurl,
                         results.canary_file)


if __name__ == '__main__':
    main()
